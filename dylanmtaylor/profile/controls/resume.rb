# Validate Resume file is being served from both good paths (symlink is working)
describe http('https://files.dylanmtaylor.com/dylan-resume.pdf') do
  its('status') { should cmp 200 }
  its('headers.Content-Type') { should cmp 'application/pdf' }
end

describe http('https://dylanmtaylor.com/files/dylan-resume.pdf') do
  its('status') { should cmp 200 }
  its('headers.Content-Type') { should cmp 'application/pdf' }
end
