describe http('https://apps.dylanmtaylor.com/') do
  its('status') { should cmp 200 }
  its('body') { should match /Application Download Center/ }
end

describe http('https://blog.dylanmtaylor.com/') do
  its('status') { should cmp 301 }
end

describe http('https://files.dylanmtaylor.com/') do
  its('status') { should cmp 403 }
  its('body') { should match /403 Forbidden/ }
end

describe http('https://fwc.dylanmtaylor.com/') do
  its('status') { should cmp 200 }
  its('body') { should match /FiOS WEP Key Calculator/ }
end

describe http('https://git.dylanmtaylor.com/') do
  its('status') { should cmp 301 }
end
