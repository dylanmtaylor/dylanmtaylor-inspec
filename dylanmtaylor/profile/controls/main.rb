# Check site is up and HTTP -> HTTPS works
describe http('http://dylanmtaylor.com/') do
  its('status') { should cmp 301 }
end

# Check that site is being served correctly on root and www DNS
describe http('https://dylanmtaylor.com/') do
  its('status') { should cmp 200 }
  its('body') { should match /Dylan M. Taylor/ }
end

describe http('https://www.dylanmtaylor.com/') do
  its('status') { should cmp 200 }
  its('body') { should match /Dylan M. Taylor/ }
end
